The Kolmogorov-Smirnov Test of Normality
Success!

Interpreting the Result

The test statistic (D), which you'll see below, provides a measurement of the divergence of your sample distribution from the normal distribution. The higher the value of D, the less probable it is that your data is normally distributed. The p-value quantifies this probability, with a low probability indicating that your sample diverges from a normal distribution to an extent unlikely to arise merely by chance. Put simply, high D, low p, is evidence that your data is not normally distributed.

It's also worth taking a look at the figures provided for skewness and kurtosis. The nearer both these are to zero, the more likely it is that your distribution is normal.

Your Data
1
0
2
0
1
1
1
1
2
2
1
1
1
2
1
0
1
2

Distribution Summary
Count : 18

Mean: 1.11111

Median: 1

Standard Deviation: 0.6764

Skewness: -0.132001

Kurtosis: -0.530816

Result: The value of the K-S test statistic (D) is .28934.

The p-value is .07909. Your data does not differ significantly from that which is normally distributed.