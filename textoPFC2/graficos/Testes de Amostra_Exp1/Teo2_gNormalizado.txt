The Kolmogorov-Smirnov Test of Normality
Success!

Interpreting the Result

The test statistic (D), which you'll see below, provides a measurement of the divergence of your sample distribution from the normal distribution. The higher the value of D, the less probable it is that your data is normally distributed. The p-value quantifies this probability, with a low probability indicating that your sample diverges from a normal distribution to an extent unlikely to arise merely by chance. Put simply, high D, low p, is evidence that your data is not normally distributed.

It's also worth taking a look at the figures provided for skewness and kurtosis. The nearer both these are to zero, the more likely it is that your distribution is normal.

Your Data
0,20 
0,00 
0,50 
0,20 
0,20 
0,25 
0,20 
0,00 
0,00 
0,33 
0,25 
0,25 
-0,25 
0,00 
0,00 
0,80 
0,00 
0,33 

Distribution Summary
Count : 36

Mean: 10.44444

Median: 0

Standard Deviation: 17.929137

Skewness: 2.1512

Kurtosis: 5.544782

Result: The value of the K-S test statistic (D) is .38934.

The p-value is .00002. This provides good evidence that your data is not normally distributed