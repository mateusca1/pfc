The Kolmogorov-Smirnov Test of Normality
Success!

Interpreting the Result

The test statistic (D), which you'll see below, provides a measurement of the divergence of your sample distribution from the normal distribution. The higher the value of D, the less probable it is that your data is normally distributed. The p-value quantifies this probability, with a low probability indicating that your sample diverges from a normal distribution to an extent unlikely to arise merely by chance. Put simply, high D, low p, is evidence that your data is not normally distributed.

It's also worth taking a look at the figures provided for skewness and kurtosis. The nearer both these are to zero, the more likely it is that your distribution is normal.

Your Data
2
3
3
3
3
4
3
4
3
5
2
4
1
4
2
2
0

Distribution Summary
Count : 17

Mean: 2.82353

Median: 3

Standard Deviation: 1.236694

Skewness: -0.522485

Kurtosis: 0.491822

Result: The value of the K-S test statistic (D) is .20553.

The p-value is .41342. Your data does not differ significantly from that which is normally distributed.