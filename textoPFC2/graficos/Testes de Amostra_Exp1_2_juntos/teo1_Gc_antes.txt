The Kolmogorov-Smirnov Test of Normality
Success!

Interpreting the Result

The test statistic (D), which you'll see below, provides a measurement of the divergence of your sample distribution from the normal distribution. The higher the value of D, the less probable it is that your data is normally distributed. The p-value quantifies this probability, with a low probability indicating that your sample diverges from a normal distribution to an extent unlikely to arise merely by chance. Put simply, high D, low p, is evidence that your data is not normally distributed.

It's also worth taking a look at the figures provided for skewness and kurtosis. The nearer both these are to zero, the more likely it is that your distribution is normal.

Your Data
1
2
0
2
0
1
1
3
3
1
1
3
2
2
2
2
2
3
3
2
0
0
1
1
0
1
1

Distribution Summary
Count : 27

Mean: 1.48148

Median: 1

Standard Deviation: 1.014145

Skewness: 0.054192

Kurtosis: -1.009898

Result: The value of the K-S test statistic (D) is .20426.

The p-value is .18265. Your data does not differ significantly from that which is normally distributed.