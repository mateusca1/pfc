The Kolmogorov-Smirnov Test of Normality
Success!

Interpreting the Result

The test statistic (D), which you'll see below, provides a measurement of the divergence of your sample distribution from the normal distribution. The higher the value of D, the less probable it is that your data is normally distributed. The p-value quantifies this probability, with a low probability indicating that your sample diverges from a normal distribution to an extent unlikely to arise merely by chance. Put simply, high D, low p, is evidence that your data is not normally distributed.

It's also worth taking a look at the figures provided for skewness and kurtosis. The nearer both these are to zero, the more likely it is that your distribution is normal.

Your Data
3
2
0
2
2
3
0
4
5
3
1
1
1
3
2
1
2
1
2
2
0
2
3
2
1
2
2
3
3
3
3
4
3
4
3
5
2
4
1
4
2
2
0
3
2
2
0
2
1
4
1
3

Distribution Summary
Count : 52

Mean: 2.23077

Median: 2

Standard Deviation: 1.262046

Skewness: 0.092563

Kurtosis: -0.355074

Result: The value of the K-S test statistic (D) is .1694.

The p-value is .08973. Your data does not differ significantly from that which is normally distributed.