\changetocdepth {4}
\babel@toc {brazil}{}
\babel@toc {brazil}{}
\contentsline {chapter}{\chapternumberline {1}Introdu\IeC {\c c}\IeC {\~a}o}{16}{chapter.1}% 
\contentsline {chapter}{\chapternumberline {2}Referencial Te\IeC {\'o}rico}{19}{chapter.2}% 
\contentsline {section}{\numberline {2.1}Grafos}{19}{section.2.1}% 
\contentsline {section}{\numberline {2.2}Demonstra\IeC {\c c}\IeC {\~a}o em Grafos}{21}{section.2.2}% 
\contentsline {subsection}{\numberline {2.2.1}Demonstra\IeC {\c c}\IeC {\~a}o Direta}{22}{subsection.2.2.1}% 
\contentsline {subsection}{\numberline {2.2.2}Demonstra\IeC {\c c}\IeC {\~a}o por Absurdo}{22}{subsection.2.2.2}% 
\contentsline {subsection}{\numberline {2.2.3}Demonstra\IeC {\c c}\IeC {\~a}o por Indu\IeC {\c c}\IeC {\~a}o}{23}{subsection.2.2.3}% 
\contentsline {section}{\numberline {2.3}Uma vis\IeC {\~a}o recursiva da indu\IeC {\c c}\IeC {\~a}o}{25}{section.2.3}% 
\contentsline {section}{\numberline {2.4}Dificuldades no ensino e aprendizagem de demonstra\IeC {\c c}\IeC {\~a}o}{25}{section.2.4}% 
\contentsline {section}{\numberline {2.5}Ensino e Aprendizagem de Grafos}{26}{section.2.5}% 
\contentsline {section}{\numberline {2.6}Sistemas de Visualiza\IeC {\c c}\IeC {\~a}o de Algoritmos (SVA)}{27}{section.2.6}% 
\contentsline {section}{\numberline {2.7}Avalia\IeC {\c c}\IeC {\~a}o de SVA}{29}{section.2.7}% 
\contentsline {subsection}{\numberline {2.7.1}M\IeC {\'e}tricas para Ganho de Aprendizagem}{29}{subsection.2.7.1}% 
\contentsline {subsubsection}{\numberline {2.7.1.1}M\IeC {\'e}trica para Ganho de Aprendizagem Absoluta}{29}{subsubsection.2.7.1.1}% 
\contentsline {subsubsection}{\numberline {2.7.1.2}M\IeC {\'e}trica para Ganho de Aprendizagem Normalizado}{30}{subsubsection.2.7.1.2}% 
\contentsline {subsection}{\numberline {2.7.2}Aspectos de Usabilidade}{30}{subsection.2.7.2}% 
\contentsline {section}{\numberline {2.8}Testes Estat\IeC {\'\i }sticos}{31}{section.2.8}% 
\contentsline {chapter}{\chapternumberline {3}Trabalhos relacionados}{34}{chapter.3}% 
\contentsline {section}{\numberline {3.1}Levantamento de Trabalhos}{34}{section.3.1}% 
\contentsline {subsection}{\numberline {3.1.1}Palavras-chaves e Strings de Busca}{34}{subsection.3.1.1}% 
\contentsline {subsection}{\numberline {3.1.2}Crit\IeC {\'e}rios de Inclus\IeC {\~a}o e Exclus\IeC {\~a}o}{35}{subsection.3.1.2}% 
\contentsline {subsection}{\numberline {3.1.3}Aspectos para An\IeC {\'a}lise dos Trabalhos}{35}{subsection.3.1.3}% 
\contentsline {section}{\numberline {3.2}An\IeC {\'a}lise dos Trabalhos Relacionados}{36}{section.3.2}% 
\contentsline {chapter}{\chapternumberline {4}SVA \textit {GraphViewer}}{40}{chapter.4}% 
\contentsline {section}{\numberline {4.1}\textit {GraphViewer}}{40}{section.4.1}% 
\contentsline {chapter}{\chapternumberline {5}Metodologia}{49}{chapter.5}% 
\contentsline {section}{\numberline {5.1}Paradigma GQM}{51}{section.5.1}% 
\contentsline {section}{\numberline {5.2}Especifica\IeC {\c c}\IeC {\~o}es do GQM}{52}{section.5.2}% 
\contentsline {chapter}{\chapternumberline {6}Resultados}{53}{chapter.6}% 
\contentsline {section}{\numberline {6.1}Resultados Subexperimento 1}{53}{section.6.1}% 
\contentsline {subsection}{\numberline {6.1.1}Descri\IeC {\c c}\IeC {\~a}o Socioacad\IeC {\^e}mica}{53}{subsection.6.1.1}% 
\contentsline {subsection}{\numberline {6.1.2}An\IeC {\'a}lise dos Resultados do Subexperimento 1}{55}{subsection.6.1.2}% 
\contentsline {subsection}{\numberline {6.1.3}Amea\IeC {\c c}as \IeC {\`a} Validade do Subexperimento 1}{60}{subsection.6.1.3}% 
\contentsline {section}{\numberline {6.2}Resultados do Subexperimento 2}{61}{section.6.2}% 
\contentsline {subsection}{\numberline {6.2.1}Descri\IeC {\c c}\IeC {\~a}o Socioacad\IeC {\^e}mica do Subexperimento 2}{61}{subsection.6.2.1}% 
\contentsline {subsection}{\numberline {6.2.2}An\IeC {\'a}lise dos Resultados do Subexperimento 2 }{63}{subsection.6.2.2}% 
\contentsline {subsection}{\numberline {6.2.3}Amea\IeC {\c c}as \IeC {\`a} Validade do Subexperimento 2}{67}{subsection.6.2.3}% 
\contentsline {section}{\numberline {6.3}Resultados do Experimento}{68}{section.6.3}% 
\contentsline {subsection}{\numberline {6.3.1}Descri\IeC {\c c}\IeC {\~a}o Socioacad\IeC {\^e}mica dos Resultados do Experimento}{68}{subsection.6.3.1}% 
\contentsline {subsection}{\numberline {6.3.2}An\IeC {\'a}lise dos Resultados do Experimento }{71}{subsection.6.3.2}% 
\contentsline {subsection}{\numberline {6.3.3}Amea\IeC {\c c}as \IeC {\`a} Validade do Experimento}{75}{subsection.6.3.3}% 
\contentsline {chapter}{\chapternumberline {7}Considera\IeC {\c c}\IeC {\~o}es Finais}{76}{chapter.7}% 
\vspace {\cftbeforepartskip }
\contentsline {chapter}{Refer\^encias}{77}{chapter*.76}% 
\cftinsert {AAA}
\renewcommand *{\cftappendixname }{ANEXO \space }
\contentsline {part}{Anexos}{80}{section*.77}% 
\contentsline {appendix}{\chapternumberline {A}Teoremas}{81}{appendix.anexochapback.1}% 
\contentsline {appendix}{\chapternumberline {B}Question\IeC {\'a}rios}{82}{appendix.anexochapback.2}% 
\contentsline {section}{\numberline {B.1}Question\IeC {\'a}rio 1 - Teorema \ref {Teorema1}}{82}{section.anexochapback.2.1}% 
\contentsline {section}{\numberline {B.2}Question\IeC {\'a}rio 2 - Teorema \ref {Teorema2}}{84}{section.anexochapback.2.2}% 
\contentsline {section}{\numberline {B.3}Question\IeC {\'a}rio 3 - Socioacad\IeC {\^e}mico}{87}{section.anexochapback.2.3}% 
\contentsline {section}{\numberline {B.4}Question\IeC {\'a}rio 4 - Usabilidade}{89}{section.anexochapback.2.4}% 
\contentsline {appendix}{\chapternumberline {C}Resultado dos \textit {p} valores}{91}{appendix.anexochapback.3}% 
